# Web Server

Digunakan untuk keperluan back-end development dengan pendekatan open source. Komponen web server yang dijelaskan yaitu sebagai berikut:

- [PHP], bahasa pemrograman web.
- [Nginx], sebagai http server.
- [MariaDB], sebagai sistem basis data.
- [phpMyAdmin], sistem basis data berbasis web.

Pada panduan perintah di web server ini menggunakan `vim` sebagai teks editor yang digunakan. Anda dapat menggantinya menggunakan `nano`, `vi`, `hx` (helix) atau teks editor lainnya yang lebih mudah.

[Nginx]:nginx.md
[PHP]:php.md
[MariaDB]:mariadb.md
[phpMyAdmin]:phpmyadmin.md
